let {fs,convertingData, convertingToUpper, convertingToLower, sortingTheLines, deletingFiles} = require('../problem2')

convertingData()
.then((data) => {
    return convertingToUpper(data)
})
.then((content)=> {
    console.log(content)
    return convertingToLower()
})
.then((content)=> {
    console.log(content)
    return sortingTheLines()
})
.then((content)=> {
    console.log(content)
    setTimeout(() => {
        deletingFiles();
    }, 2000);
})
.catch((error)=> {
    console.log("error is: ", error)
})

